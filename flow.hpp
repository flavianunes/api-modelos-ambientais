#ifndef FLOW_HPP
#define FLOW_HPP

#include "system.hpp"


class Flow {
public:

	virtual void setSource(System* s) = 0;
	virtual System* getSource() = 0;

	virtual void setDestination(System* d) = 0;
	virtual System* getDestination() = 0;

	virtual void setName (string n) = 0;
	virtual string getName() = 0;

	//calcula a quantidade de flow que irá da source para desination
	virtual double execute() = 0;

	virtual ~Flow(){}

};

#endif
