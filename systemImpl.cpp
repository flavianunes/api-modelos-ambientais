#include "systemImpl.hpp"
#include <iostream>

using namespace std;


SystemImpl::SystemImpl(const SystemImpl& s) {

	//para possibilitar Sysyem a = new System(b)

	if (this == &s)
		return;
	this->setName(s.getName());
	this->setResource(s.getResource());
}

System& SystemImpl::operator=(const System& s) {

	/**
	 * para possibilitar a = b, e no lugar de fazer
	 * ambas variaveis apontarem para o mesmo endereço,
	 * copiar os valores dos atributos da "b" para "a"
	 */

	if (this == &s) 
		return *this;

	this->setName(s.getName());
	this->setResource(s.getResource());
	return *this;
}


SystemImpl::SystemImpl(string n) {
	name = n;
}

SystemImpl::SystemImpl(string n, double r) {
	name = n;
	resource = r;
}

double SystemImpl::getResource() const {
	return resource;
}

void SystemImpl::setResource(double r) {
	this->resource = r;
}

string SystemImpl::getName() const {
	return name;
}

void SystemImpl::setName(string n) {
	this->name = n;
}

SystemImpl::~SystemImpl() {
}
