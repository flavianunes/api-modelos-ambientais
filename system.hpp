#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <string>

using namespace std;

class System {
public:

	virtual double getResource() const = 0;
	virtual void setResource(double r) = 0; 

	virtual string getName() const = 0;
	virtual void setName(string n) = 0; 

	virtual	System& operator=(const System& s) = 0;

	virtual ~System(){}
};

#endif
