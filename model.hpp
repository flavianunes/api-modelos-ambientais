#ifndef MODEL_HPP
#define MODEL_HPP

#include "system.hpp"
#include "flow.hpp"

#include <vector>

class Model{
public:

	typedef vector<System*>::iterator systemIt;
	typedef vector<Flow*>::iterator flowIt;


	template<class T>
	Flow* createFlow(string n, System *src, System *dst) {
		Flow *f = new T(n, src, dst);
		addFlow(f);		
		return f;
	}

	//executa o modelo, alterando todos os systems necessários
	virtual void execute(float startTime, float endTime) = 0;

	//method factory
	virtual System* createSystem(string n, double r) = 0;
	static Model* createModel(string n);

	virtual string getName() const = 0;
	virtual void setName(string n) = 0;

	virtual systemIt systemBegin() = 0;
	virtual systemIt systemEnd() = 0;

	virtual flowIt flowBegin() = 0;
	virtual flowIt flowEnd() = 0;

	virtual ~Model()  {}

private:
	virtual void addFlow(Flow* flow) = 0;	
	virtual void addSystem(System* system) = 0;
};





#endif
