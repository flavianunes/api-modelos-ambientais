#include "systemImpl.hpp"
#include "flowImpl.hpp"
#include "modelImpl.hpp"

#include <cstdio>
#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

void print() {
	for (int i=0; i<50; i++)
		cout<<"_";
}

void systemTest() {

	print();
	cout<<endl<<"Teste unitário System:"<<endl;

	System *s = new SystemImpl ("PrimeiroSistema");

	cout<<"		Teste construtor: ";
	assert(s->getResource() == 0);
	cout<<"OK!"<<endl;

	cout<<"		Teste set/get resource: ";
	s->setResource(100);
	assert(s->getResource() == 100);
	cout<<"OK!"<<endl;

	cout<<"		Teste operador = : ";
	System *s2 = new SystemImpl ("SegundoSistema", 200);
	*s = *s2;
	assert(s2->getResource() == 200);
	assert(s->getResource() == 200);
	cout<<"OK!"<<endl;

	delete s; 
	delete s2;
	

	cout<<"		Teste construtor de cópia: ";
	s = new SystemImpl("PrimeiroSistema", 150);
	s2 = new SystemImpl(*dynamic_cast<SystemImpl*>(s));

	assert(s2->getResource() == 150);
	assert(s->getResource() == 150);
	cout<<"OK!"<<endl;

	delete s;
	delete s2;

	print();
	cout<<endl<<endl;

}

void flowTest() {

	print();
	cout<<endl<<"Teste unitário Flow:"<<endl;

	//implementação do execute
	class FlowTest : public FlowImpl {
	public:
		FlowTest (string n, System* s = NULL, System* d = NULL) : FlowImpl (n,s,d) { }
		double execute() {
			if (this->getSource() == NULL)
				return 0;
			return this->getSource()->getResource()/2; //50%
		}
	};

	System *s = new SystemImpl ("PrimeiroSistema", 10);
	System *s2 = new SystemImpl ("SegundoSistema", 20);

	cout<<"		Teste construtor: ";
	Flow *f = new FlowTest("Flow1", s, s2);
	assert(f->getSource() == s);
	assert(f->getDestination() == s2);
	assert(f->getName() == "Flow1");
	cout<<"OK!"<<endl;

	
	cout<<"		Teste get/set: ";
	Flow *f2 = new FlowTest("Flow 1", NULL, NULL);
	f2->setSource(s2);
	f2->setDestination(s);
	f2->setName("Flow 2");
	assert(f2->getSource() == s2);
	assert(f2->getDestination() == s);
	assert(f2->getName() == "Flow 2");
	cout<<"OK!"<<endl;

	cout<<"		Teste execute: ";
	assert(f2->execute() == 10); 
	cout<<"OK!"<<endl;

	delete f;
	delete f2;

	delete s;
	delete s2;

	print();
	cout<<endl<<endl;

}


void modelTest() {

	print();
	cout<<endl<<"Teste unitário Model:"<<endl;

	//implementação do execute
	class FlowTest : public FlowImpl {
	public:
		FlowTest(string n, System* s = NULL, System* d = NULL) : FlowImpl(n,s,d) { }

		double execute() {
			if (source == NULL)
				return 0;
			return source->getResource() / 2; 
		}
	};


	Model *m = Model::createModel("modelo 1");

	System *s1 = m->createSystem("System 1", 200);
	System *s2 = m->createSystem("System 2", 100);
	Flow *f1 =  m->createFlow<FlowTest>("Flow 1", s1, s2);

	
	cout<<"		Teste execute: ";	
	m->execute(0, 2); // 2 iterações
	
	assert(s1->getResource() == 50);
	assert(s2->getResource() == 250);

	cout<<"OK!"<<endl;

	delete m;
}

void functionalTest() {
	//teste funcional
}

int main() {
	systemTest();
	flowTest();
	modelTest();
	functionalTest();
	return 0;
}
