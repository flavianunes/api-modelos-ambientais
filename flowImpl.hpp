#ifndef FLOWIMPL_HPP
#define FLOWIMPL_HPP

#include "system.hpp"
#include "flow.hpp"


class FlowImpl : public Flow {
public:

	FlowImpl();
	FlowImpl (string n, System* s = NULL, System* d = NULL );

	void setSource(System* s);
	System* getSource();

	void setDestination(System* d);
	System* getDestination();

	void setName (string n);
	string getName();

	virtual double execute() = 0;

	~FlowImpl();
	

protected:
	string name;
	System* source;
	System* destination;

};

#endif
