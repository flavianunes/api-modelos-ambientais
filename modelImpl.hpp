#ifndef MODELIMPL_HPP
#define MODELIMPL_HPP

#include "system.hpp"
#include "flow.hpp"
#include "model.hpp"

class ModelImpl : public Model {
public:

	
	typedef vector<System*>::iterator systemIt;
	typedef vector<Flow*>::iterator flowIt;


	ModelImpl(string Name);
	//construtor de cópia
	ModelImpl(const ModelImpl& m);

	/*operador =*/
	ModelImpl& operator=(const ModelImpl& m);

	/*metodo de execução*/
	void execute(float startTime, float endTime);

	/* method factory*/
	System* createSystem(string n, double r);

	static Model* createModel(string n);

	/*metodos get/set*/
	string getName() const;
	void setName(string n);

	/*inicio e fim do vetor de system*/
	systemIt systemBegin();
	systemIt systemEnd();

	/*inicio e fim do vetor de flow*/
	flowIt flowBegin();
	flowIt flowEnd();

	~ModelImpl();


private:

	/*metodos add*/
	void addFlow(Flow* f);
	void addSystem(System* s);

	string name;

	vector<System*> systemContainer;
	vector<Flow*> flowContainer;

	vector<vector<double> > valuesRegistry;

	static vector<Model*> modelVector;

};

Model* createModel(string n);

#endif
