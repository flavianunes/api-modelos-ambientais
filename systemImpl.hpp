#ifndef SYSTEMIMPL_HPP
#define SYSTEMIMPL_HPP

#include "system.hpp"


class SystemImpl : public System {
public:

	SystemImpl ();
	SystemImpl (string n);
	SystemImpl (string n, double r);
	//construtor de cópia
	SystemImpl (const SystemImpl& s); 

	//operador =
	System& operator=(const System& s);

	
	double getResource() const;
	void setResource(double r); 

	string getName() const;
	void setName(string n); 

	virtual ~SystemImpl();
	

protected:
	string name;
	double resource;

};

#endif
