#include "flowImpl.hpp"

FlowImpl::FlowImpl(string n, System* s, System* d) {
	setName(n);
	setSource(s);
	setDestination(d);
}


void FlowImpl::setSource(System* s) {
	source = s;
}

System* FlowImpl::getSource() {
	return source;
}

void FlowImpl::setDestination(System* d) {
	destination = d;
}

System* FlowImpl::getDestination() {
	return destination;
}

void FlowImpl::setName(string n) {
	name = n;
}

string FlowImpl::getName() {
	return name;
}

FlowImpl::~FlowImpl() { }
