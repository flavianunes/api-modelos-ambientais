#include "modelImpl.hpp"
#include "systemImpl.hpp"
#include <cstdio>

//vetor de model instanciado em escopo global
vector<Model*>  ModelImpl::modelVector;

ModelImpl::ModelImpl(string n) {
	name = n;
}

ModelImpl::ModelImpl(const ModelImpl& m) {
	flowContainer = m.flowContainer;
	systemContainer = m.systemContainer;
	name = m.name;
}

ModelImpl& ModelImpl::operator=(const ModelImpl& m) {
	if (this == &m)
		return *this;
	setName(m.getName());
	flowContainer.assign(m.flowContainer.begin(), m.flowContainer.end());
	systemContainer.assign(m.systemContainer.begin(), m.systemContainer.end());
	valuesRegistry.assign(m.valuesRegistry.begin(), m.valuesRegistry.end());

	return *this;
}

ModelImpl::~ModelImpl() { 
	for (int i = 0; i < systemContainer.size(); i++)
       delete systemContainer[i];

   for (int i = 0; i < flowContainer.size(); i++)
       delete flowContainer[i];

   systemContainer.clear();
   flowContainer.clear();
   ModelImpl::modelVector.clear();

}

string ModelImpl::getName() const {
	return name;
}

void ModelImpl::setName(string n) {
	name = n;
}

void ModelImpl::addFlow(Flow* f) {
	flowContainer.push_back(f);
}

void ModelImpl::addSystem(System* s) {
	systemContainer.push_back(s);
}

/**
 * METHOD FACTORY
 */
System*  ModelImpl::createSystem(string n, double r) {
	System *s = new SystemImpl (n, r);
	systemContainer.push_back(s);

	return s;
}

Model* Model::createModel(string n) {
	return ModelImpl::createModel(n);
}


Model* ModelImpl::createModel(string n) {
	Model *m = new ModelImpl(n);
	ModelImpl::modelVector.push_back(m);
	return m;
}

/**
 * FIM METHOD FACTORY
 */

void ModelImpl::execute(float startTime, float endTime ) {
	int nIterations = (endTime - startTime);
	vector<double> flowAmount = vector<double>(flowContainer.size(), 0);
	valuesRegistry.assign(systemContainer.size(),vector<double>(nIterations, 0)); //matriz

	for (int i=0; i<nIterations; i++) {

		for (size_t j=0; j<flowContainer.size(); j++)
			flowAmount[j] = flowContainer[j]->execute(); 

		for (size_t j=0; j<flowContainer.size(); j++) {

			System *src = flowContainer[j]->getSource();
			System *dst = flowContainer[j]->getDestination();

			if (src != NULL) {
				src->setResource(src->getResource() - flowAmount[j]);
				
			}
			if (dst != NULL)
				dst->setResource(dst->getResource() + flowAmount[j]);
		}

		for (size_t j=0; j<systemContainer.size(); j++) {
			valuesRegistry[j][i] = systemContainer[j]->getResource();
		}

	}
}

ModelImpl::systemIt ModelImpl::systemBegin() {
	return systemContainer.begin();
}

ModelImpl::systemIt ModelImpl::systemEnd() {
	return systemContainer.end();
}

ModelImpl::flowIt ModelImpl::flowBegin() {
	return flowContainer.begin();
}

ModelImpl::flowIt ModelImpl::flowEnd() {
	return flowContainer.end();
}


